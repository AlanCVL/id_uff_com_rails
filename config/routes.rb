Rails.application.routes.draw do
  post '/login', to:'session#login'
  post '/sign_up', to: 'register#sign_up'
  get '/current_user', to: 'application#user_must_exist'
  resources :teams
  resources :disciplines
  resources :teachers
  resources :students
  resources :users, except: [:create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

  #No insomnia, começa com
  # POST/JSON : Sign_up e insere {user:{email, password, password_confirmation}},
  # POST/Json : Login e insere {user: {email, password que foram colocados no sign_up}} e recebra um token de presente :)
  # GET/body : current_user e insira no header o Authorization e o token