class CreateTeacherDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :teacher_disciplines do |t|
      t.references :teacher, foreign_key: true
      t.references :discipline, foreign_key: true

      t.timestamps
    end
  end
end
