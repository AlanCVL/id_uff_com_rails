class AddTeacherToTeam < ActiveRecord::Migration[5.2]
  def change
    add_reference :teams, :teacher, foreign_key: true
  end
end
