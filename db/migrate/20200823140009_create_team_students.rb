class CreateTeamStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :team_students do |t|
      t.references :team, foreign_key: true
      t.references :student, foreign_key: true
      t.float :p1
      t.float :p2
      t.float :status
      t.float :finalgrade

      t.timestamps
    end
  end
end
