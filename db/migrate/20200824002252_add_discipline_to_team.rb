class AddDisciplineToTeam < ActiveRecord::Migration[5.2]
  def change
    add_reference :teams, :discipline, foreign_key: true
  end
end
