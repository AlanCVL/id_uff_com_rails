class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :initials
      t.string :schedule
      t.string :status
      t.string :weekday

      t.timestamps
    end
  end
end
