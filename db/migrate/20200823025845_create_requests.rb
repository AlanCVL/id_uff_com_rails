class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.integer :pre_id
      t.integer :pos_id

      t.timestamps
    end
  end
end
