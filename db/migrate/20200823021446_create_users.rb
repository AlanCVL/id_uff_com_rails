class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.integer :kind, default: 0
      t.references :userable, polymorphic: true

      t.timestamps
    end
  end
end
