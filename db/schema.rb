# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

<<<<<<< HEAD
<<<<<<< HEAD
ActiveRecord::Schema.define(version: 2020_08_23_145041) do
=======
ActiveRecord::Schema.define(version: 2020_08_23_225628) do
>>>>>>> Models
=======
ActiveRecord::Schema.define(version: 2020_08_24_002252) do
>>>>>>> Migration

  create_table "disciplines", force: :cascade do |t|
    t.string "department"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "requests", force: :cascade do |t|
    t.integer "pre_id"
    t.integer "pos_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.float "cr"
    t.string "registration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teacher_disciplines", force: :cascade do |t|
    t.integer "teacher_id"
    t.integer "discipline_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_teacher_disciplines_on_discipline_id"
    t.index ["teacher_id"], name: "index_teacher_disciplines_on_teacher_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "registration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_students", force: :cascade do |t|
    t.integer "team_id"
    t.integer "student_id"
    t.float "p1"
    t.float "p2"
    t.float "status"
    t.float "finalgrade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_team_students_on_student_id"
    t.index ["team_id"], name: "index_team_students_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "initials"
    t.string "schedule"
    t.string "status"
    t.string "weekday"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "teacher_id"
    t.integer "discipline_id"
    t.index ["discipline_id"], name: "index_teams_on_discipline_id"
    t.index ["teacher_id"], name: "index_teams_on_teacher_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password"
    t.integer "kind", default: 0
    t.string "userable_type"
    t.integer "userable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "password_confirmation"
    t.index ["userable_type", "userable_id"], name: "index_users_on_userable_type_and_userable_id"
  end

end
