class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :kind
end
