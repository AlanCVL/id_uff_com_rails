class Team < ApplicationRecord
    has_many :team_students
    has_many :students, through: :team_students
    belongs_to :teacher
    belongs_to :discipline
end
