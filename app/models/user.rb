class User < ApplicationRecord
    validates :email, format: { with: /\b[A-Z0-9._%a-z\-]+@id.uff\.br\z/, message: "must be a id.uff.br account" }
    validates :password, length: { minimum: 6, maximum: 15} 
    # belongs_to :userable, :polymorphic => true
    has_secure_password
    # before_save :create_student
    
    # def create_student
    #     @user = User.new(user_params)
    #     if @user.kind == student
    #         byebug
    #         puts '------TESTE-------'
    #         #a=  Student.cretae
    #         # @student = Student.new( params[:user], registration: '12345')
    #         # #userable =  
    #         # @student.user.create(user)
    #     end
    # end
    validates :email, presence: true, uniqueness: true
    validates :email, length: {minimum:13}
    validates :name, presence: true, length: {minimum:3}
    validates_format_of :name, :with =>  /\A[a-z]+\z/i #apenas letras no nome

    enum kind: {
        student: 0,
        secretary: 1,
        teacher: 2,
        admin: 3
    }
end
