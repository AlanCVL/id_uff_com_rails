class Student < ApplicationRecord
    validates :registration, uniqueness: true
    # has_one :user, :as => :userable
    has_many :team_students
    has_many :teams, through: :team_students
end
