class JsonWebToken
    Secret = ENV["SECRET_KEY_BASE"]
    
    # encriptografa user
    def self.encode(payload)
        JWT.encode(payload, Secret)
    end

    #descpitografa user
    def self.decode(token)
        # byebug
        begin

            decoded = JWT.decode(token, Secret)
        rescue => exception
            return nil
        end
    end


end