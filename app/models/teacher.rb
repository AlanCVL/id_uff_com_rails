class Teacher < ApplicationRecord
    has_many :teams
    
    has_many :teacher_disciplines
    has_many :disciplines, through: :teacher_disciplines

    name
end
