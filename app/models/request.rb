class Request < ApplicationRecord
    validate :differents_pre_and_pos
    #pre e pos pertence ao model Discipline
    belongs_to :pre, class_name: 'Discipline'
    belongs_to :pos, class_name: 'Discipline'


    #Caso os ids de pos e pre sejam iguais será bloqueado
    def differents_pre_and_pos
        if pre == pos
            errors.add("pre and pos can't be the same ids")
        end
    end
end
