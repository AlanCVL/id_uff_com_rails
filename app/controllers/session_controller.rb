class SessionController < ApplicationController
  def login
    
    #pega email do user_id
    @user = User.find_by(email: params[:user][:email])
    #compara se essa password é a desse user_id
    @user = @user&.authenticate(params[:user][:password])

    #se a password for a mesma, user se torna o user_id
    if @user
      token = JsonWebToken.encode(user_id: @user.id)
      render json: {token: token, user: @user}

    #se nao for, a mostra unauthorized  
    else
      render json: {error: "unauthorized"}
    end
  end
end
